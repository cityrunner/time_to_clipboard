from datetime import datetime, timedelta
from zoneinfo import ZoneInfo
from platform import system
from subprocess import check_call


def string_to_clipboard(string: str) -> None:
    # Linux requires xclip installed
    systems = {'Linux': '| xclip -selection clipboard',
               'Darwin': '|pbcopy',
               'Windows': '|clip'}
    process = systems[system()]
    command = 'echo ' + string.strip() + process
    print(f"'{string}' will be copied to clipboard if there'll no shell errors")
    check_call(command, shell=True)


def get_working_hours(working_hours: int = 8,
                      break_hours: int = 1,
                      round_by: int = 5,
                      time_zone=None) -> str:
    working_day_length = working_hours + break_hours
    time_start = datetime.now()
    print(f"Local time is {time_start.strftime('%H:%M')}")
    if time_zone:
        time_start = time_start.astimezone(ZoneInfo(time_zone))
    time_start += timedelta(minutes=round_by - 1)
    time_start -= timedelta(minutes=time_start.minute % round_by)
    time_end = time_start + timedelta(hours=working_day_length)
    working_time = f"{time_start.strftime('%H:%M')} - {time_end.strftime('%H:%M')}"
    print(f"Working time is {working_time}")
    return working_time


if __name__ == '__main__':
    work_hours = get_working_hours(time_zone='Europe/Moscow')
    string_to_clipboard(work_hours)
