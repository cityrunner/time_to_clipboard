# Put time interval to clipboard

## Description
Simple script to put your working hours to clipboard.

## Usage
Just run time_to_clipboard.py and then press ctrl+v to paste time interval string.

## Dependencies
Linux requires xclip installed
